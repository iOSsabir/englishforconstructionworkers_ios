//
//  Sing In ViewController.swift
//  EnglishForConstructionArena
//
//  Created by Macbook on 10/03/2020.
//  Copyright © 2020 Shahid Sabir. All rights reserved.
//

import UIKit
import GoogleSignIn


class Sing_In_ViewController: UIViewController {
    
    @IBOutlet weak var signinImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    
    @IBAction func connectFacebookBtn(_ sender: Any) {
    }
    
    @IBAction func connectGoogleBtn(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        
      
    }
    

    @IBAction func loginEmailBtn(_ sender: Any) {
        
        
        performSegue(withIdentifier: "signintologin", sender: nil)
    }
}
